'use strict';
$(function(){
    var calculadora = new Calculadora('num','oper_val','acc_val');
    $('.btn_oper').on('click',function(){
        calculadora.ejecutar_operacion($(this));
    });
    $('#calcular').on('click',function(){
        calculadora.calcular()
    });
    $('#desde_mem').on('click',function(){
        $('#num').html($('#memoria').html());
    });
    $('#a_mem').on('click',function(){
        $('#memoria').html($('#num').html());
    });

    //Eventos DRAG & DROP
    $('.draggable').disableSelection();
    $('.draggable').draggable({
        zIndex: 2500,
        revert: 'invalid',
        helper:'clone'
    });
    $('.droppable').droppable({
        accept: ".draggable",
        activeClass:'on-droppable',
        drop: function( event, ui ) {
            $(this).html(ui.helper.html());
            $(this).addClass('on-dropped').delay(500).queue(function(){
                $(this).removeClass('on-dropped').dequeue();
            });
        }
    });
});
