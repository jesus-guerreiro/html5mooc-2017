'use strict';
/**
 * Clase para implementar una calculadora básica
 * @param {string} num  id del input donde se introducen los números
 * @param {string} oper id del span donde se muestra la operación a realizar
 * @param {stirng} acc  id del span donde se guarda el valor introducido anteriormente
 */
function Calculadora(num,oper,acc) {
    this.num = $('#'+num);
    this.oper_txt = $('#'+oper);
    this.acc_txt = $('#'+acc);
    this.op = '';
    this.acc = 0;
}

/**
 * Vacía el input donde se escribe
 * @return {void}
 */
Calculadora.prototype.vaciar = function() {
    this.num.val('');
}

/**
 * Guarda el valor introducido antes
 * @param stirng valor  el valor que es acaba de introcir
 * @return {void}
 */
Calculadora.prototype.set_acc = function(valor) {
    this.acc = valor;
    this.acc_txt.html(valor);
}

/**
 * Ejecuta la operación indica en el atributo op
 * @return {void}
 */
Calculadora.prototype.calcular = function() {
    var cantidad = this.num.val();
    switch (this.op) {
        case '+':
            this.num.val((+this.acc + +cantidad));
            break;
        case '-':
            this.num.val((+this.acc - +cantidad));
            break;
        case '*':
            this.num.val((+this.acc * +cantidad));
            break;
        case '/':
            this.num.val((+this.acc / +cantidad));
            break;
        case '^':
            this.num.val(Math.pow(this.acc,cantidad));
            break;
        case '^2':
            this.num.val(Math.pow(cantidad,2));
            break;
        case 'pot2':
            var resultado = 1;
            +cantidad;
            while (cantidad-- > 0) {
                resultado *= 2;
            }
            this.num.val(resultado);
            break;
        case 'fact':
            var resultado = +cantidad;
            while (cantidad-- > 1) {
                resultado *= cantidad;
            }
            this.num.val(resultado);
            break;
        case 'inv':
            this.num.val(1/cantidad);
            break;
        case 'sqrt':
            this.num.val(Math.sqrt(cantidad));
            break;
        case 'ent':
            this.num.val(+cantidad >=0 ? Math.floor(cantidad):Math.ceil(cantidad));
            break;
        case 'sum':
            var numeros = cantidad.split(',');
            var total = 0;
            numeros.forEach(function(elem,ind,arr){
                total += +elem;
            });
            this.num.val(total);
            break;
        case 'prod':
            var numeros = cantidad.split(',');
            var total = 1;
            numeros.forEach(function(elem,ind,arr){
                total *= +elem;
            });
            this.num.val(total);
            break;
    }
}

/**
 * Actualiza el estado de la calculadora con los datos de la operación a realizar.
 * Ejecutará el cálculo automáticamente si el botón está asociado a una operación unitaria.
 * @param  {button} btn_oper botón que se pulsó y que indica la operación a realizar.
 * @return {void}
 */
Calculadora.prototype.ejecutar_operacion = function(btn_oper) {
    var nueva_operacion = btn_oper.data('op');
    var autocalcular = btn_oper.data('tipo') === 'unit' ? true : false;
    this.set_acc(this.num.val());
    this.op = nueva_operacion;
    this.oper_txt.html(btn_oper.html());
    if (autocalcular){
        this.calcular();
    }
}
