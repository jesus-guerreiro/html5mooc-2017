'use sctrict';
if (typeof Galeria === "undefined" ) {
    throw new Error("No se ha cargado la galería");
}

/**
 * Muestra un mensaje en la parte inferior derecha
 * @param  {string} txt_mensaje mesaje que se quiere mostrar.
 * @param  {int} tiempo      que se muestra el mensaje (en milisegundos)
 */
function mostrar_mensaje(txt_mensaje, tiempo) {
    var msg = $('#mensajes');
    msg.clearQueue().hide(); //Si se está mostrando, elimina el resto de evento y la oculta.
    tiempo = typeof tiempo !== 'undefined' ? tiempo : 3000;
    msg.html(txt_mensaje);
    msg.fadeIn().delay(tiempo).fadeOut();
}

/**
 * galería de imágenes
 * @type {Galeria}
 */
var galeria = new Galeria({
    enlaces:'.nav-slider',
    info_persona:'#persona',
    frase:'#frase',
    foto:'#foto',
    form_frase:'#frase_d',
    form_foto:'#foto_d',
    form_persona:'#persona_d',
    form_div:'#datos',
    botonera:'#selector',
    tiempo:3,
});

$(function (){
    galeria.generar_botonera();
    galeria.select(0);

    $('#selector').on('click','.nav-slider',function(){
        galeria.select($(this).data('cont_elem'));
    });

    $("#desp_form").on("click", function(){
        galeria.editar_cita();
    });

    $('#borrar').on('click',function(){
        if (galeria.eliminar_cita()) {
            mostrar_mensaje('Cita eliminada',2000);
        }
    });

    $('#guardar').on('click',function(){
        galeria.actualizar_cita();
        mostrar_mensaje('Cita actualizada',2000);
    });

    $("#nuevo").on("click", function(){
        galeria.nueva_cita();
        mostrar_mensaje('Cita añadida',2000);
    });

    $('#reset_db').on('click', function(){
        if (confirm('¿Seguro que desea borrar los datos actualuales y restaurar la base de datos original?')) {
            galeria.restaurar_citas();
            mostrar_mensaje('Base de datos restaurada correctamente');
        } else {
            mostrar_mensaje('Operación cancelada');
        }
    });
    $('#mensajes').on('click', function(){
        $(this).hide();
    });
});
