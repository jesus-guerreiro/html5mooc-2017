'use sctrict';
if (typeof jQuery === "undefined" && Zepto === 'undefined') {
    throw new Error("Cronómetro requiere jQuery");
}

function Cronometro(disp) {
    this.cl = $(disp);
    this.t;
};

Cronometro.prototype.mostrar = function() {
    this.cl.text(parseFloat(+this.cl.text()+0.1).toFixed(1));
};

Cronometro.prototype.arrancar = function() {
    var self = this;
    this.t=setInterval(function() {
        self.mostrar();
    },100);
};

Cronometro.prototype.parar = function() {
    clearInterval(this.t);
    this.t=undefined;
};

Cronometro.prototype.cambiar = function() {
    if (!this.t) {
        this.arrancar();
    } else {
        this.parar();
    }
};

Cronometro.prototype.reset = function() {
    this.cl.html('0.0');
};